// initialisation
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Note = require('./models/noteModel');
app.use(express.json()); // converting request body to json
// connection to server mongo data base
mongoose.connect('mongodb+srv://naushadhusain:Jarina%4004@cluster0.m1rropf.mongodb.net/notesdb').then(() => {
    console.log('connected to mongodb');
    // App routes

    // Create
    app.post('/note/create', async (req, resp) => {
        console.log(req.body.id);
        await Note.deleteOne({ id: req.body.id })
        const newNotes = new Note(req.body);
        const result = await newNotes.save()
        console.log("inserted data " + result);
        resp.send(result);
    });
    // Read
    app.get('/note/list/', async (req, resp) => {
        console.log("list of data ");
        const data = await Note.find();
        console.log("list of data " + data);
        resp.send(data);
    });
    // Delete
    app.delete('/note/delete/:id', async (req, resp) => {
        const result = await Note.deleteOne({ id: req.params.id });
        resp.send(result);
    });
    // Update
    app.put('/note/update/:id', async (req, resp) => {
        const result = await Note.updateOne({ id: req.params.id }, { $set: { id: req.params.id, userid: req.body.userid, title: req.body.title, content: req.body.content } });
        resp.send(result);
    });
    //Search
    
    app.get('/note/saerch/:id', async (req, resp) => {
        const result = await Note.find({
            "$or": [
                { userid: req.params.id },
                { id: req.params.id },
            ]
        });
        resp.send(result);
    });
});

// Port listen ON PORT
const PORT=process.env.PORT || 5000;
app.listen(5000, () => {
    console.log('server is running on this Port:5000');
});
